# Simple Dashboard Maker

This is a simple dashboard maker. The dashboard lives in your local browser storage.

## Quick Setup

1. Go to the [demo](https://barefootstache.codeberg.page/Simple-Dashboard-Maker/@pages/) page.
2. Setup your *sections* and *apps* by accessing the *settings* button at the top right.
    - Make sure to save the changes to get a refresh
3. Bookmark the page and your dashboard is ready to use.

## Custom Local icon images

If you want custom local images, then

1. Clone/Download the repo.
2. Add the images to `./assets/icons/`.
3. In the *Icon image* field use the `./assets/icons/<file-name>.<file-extension>` URL.
4. Create a bookmark that is linked to the `index.html` file.

## Save Local Copy

Once finished designing your dashboard, to save a local copy 

1. Go to the *DOM and Style Inspector* (`Ctrl+Shift+C`) (Firefox).
2. Navigate to the opening `<html>` tag then right-click > Copy > Outer HTML.
3. Open a text file and paste it. Save it as `index.html`.
4. Create a bookmark that is linked to the `index.html` file.

### Optionally

You can copy-paste the templates into the `./templates/local/` directory. These will be saved locally only.

Then run `pwd` in the `local` directory to get the path of working directory.

The bookmark link is then 

```
file://<pwd>/<template>.html
```

or from the project level

```
file://<pwd>/Simple-Dashboard-Maker/templates/local/<template>.html
```

## Templates

There are premade [templates](./templates/templates.md) that can quickly be used with small tweaks.

## TODO

- [ ] customize page name and icon
- [ ] auto-generate static site
- [ ] save icons in `assets` directory with prefilled fields
- [ ] create multiple pages
- [ ] quick links to the pages
- [ ] modify templates via form
- [ ] the in browser builder needs better anchor tag parsing: `title` and `rel` properties, also `.site-title > span` needs the `title` property
