# Templates

## Open Street Map

Make sure to add your user name. 

### Option 1: Locally

Update the HTML file with your `<osm-user-name>`.

### Option 2: Remotely

Append your `<osm-user-name>` to the URL 

``` 
https://barefootstache.codeberg.page/Simple-Dashboard-Maker/@pages/templates/openstreetmap.html?username=<osm-user-name>
``` 

[Demo](https://barefootstache.codeberg.page/Simple-Dashboard-Maker/@pages/templates/openstreetmap.html)

## Socials

Make your own social media portal.
