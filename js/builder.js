function buildApp(app) {
  const example = {
    link: 'https://example.com/',
    image: 'https://upload.wikimedia.org/wikipedia/commons/a/a9/Example.jpg',
    title: 'Example'
  }

  return `
  <!-- ${app.title} -->
  <li class="site">
    <a href="${app.link || example.link}" target="_blank">
      <div class="tile">
        <div class="site-icon" style="background-image: url('${app.image || example.image}')"></div>
      </div>
      <div class="site-title">
        <span>${app.title || example.title}</span>
      </div>
    </a>
  </li>
  `;
}

function buildApps(apps) {
  let htmlList = '<ul class="section-list">';
  apps.forEach(el => {
    htmlList += buildApp(el);
  });
  htmlList += '</ul>';
  return htmlList;
}

function buildFormSection(sections, pageId) {
  let htmlForm = '';

  sections.forEach(section => {
    htmlForm += `<form id="sections[${section.id}]"></form>`;
    section.apps.forEach(app => {
      htmlForm += `<form id="sections[${section.id}].apps[${app.id}]"></form>`;
    });
  });

  htmlForm += '<!-- Fields -->';

  sections.forEach((section, jj) => {
    htmlForm += `
<label class="section-title" for="sectionTitle[${section.id}]">Section title</label> 
<input 
    class="form-input-section" 
    type="text" 
    name="sectionTitle[${section.id}]" 
    form="sections[${section.id}]" 
    value="${section.title}"
    placeholder="Name"
  ><br>
<hr>`
    section.apps.forEach((app, ii) => {
      htmlForm += `
<div class="form-app">
  <label for="appTitle[${section.id}][${app.id}]">App title</label> 
  <input 
    class="form-input" 
    type="text" 
    name="appTitle[${section.id}][${app.id}]" 
    placeholder="Name"
    value="${app.title}"
    form="sections[${section.id}].apps[${app.id}]"
  ><br>
  <label for="appLink[${section.id}][${app.id}]">App link</label> 
  <input 
    class="form-input" 
    type="text" 
    name="appLink[${section.id}][${app.id}]" 
    placeholder="example.com" 
    value="${app.link}"
    form="sections[${section.id}].apps[${app.id}]"
  ><br>
  <label for="appImage[${section.id}][${app.id}]">App image</label> 
  <input 
    class="form-input" 
    type="text" 
    name="appImage[${section.id}][${app.id}]" 
    placeholder="file.example.com"
    value="${app.image}"
    form="sections[${section.id}].apps[${app.id}]"
  ><br>
  <div class="app-buttons">
    <input 
      class="button-error" 
      type="button" 
      name="appDelete[${section.id}][${app.id}]" 
      value="${section.apps.length > 1 ? 'Delete' : 'Reset'} App" 
      title="Delete '${app.title}' !?"
      form="sections[${section.id}].apps[${app.id}]"
      onclick="deleteApp(${pageId},${section.id},${app.id})"
    >
    <input 
      class="button-success" 
      type="button" 
      name="appAdd[${section.id}][${app.id}]" 
      value="Add App" 
      form="sections[${section.id}].apps[${app.id}]"
      onclick="addApp(${pageId},${section.id})"
    >
  </div>
</div>`;
      if (ii < section.apps.length - 1) {
        htmlForm += '<hr class="icon-border-dashed">';
      }
    });

    htmlForm += `
<hr>
<div class="app-buttons">
<input 
  class="button-error" 
  type="button" 
  name="sectionDelete[${section.id}]" 
  value="${sections.length > 1 ? 'Delete' : 'Reset'} Section" 
  title="Delete '${section.title}' !?"
  form="sections[${section.id}]"
  onclick="deleteSection(${pageId},${section.id})"
>
<input 
  class="button-success" 
  type="button" 
  name="sectionAdd[${section.id}]" 
  value="Add Section" 
  form="sections[${section.id}]"
  onclick="addSection(${pageId})"
>
</div>
`;
    if (jj < sections.length - 1) {
      htmlForm += '<hr>';
    }
  });

  return htmlForm;
}

function buildSection(section) {
  return `<!-- ${section.title} -->
  <section>
    <h3 class="section-title">${section.title}</h3>
    ${buildApps(section.apps)}
  </section>
  `;
}


Builder = {
  header: function (data) {
    let htmlHeader = `
<header class="logo-and-wordmark">
  <div class="logo-das">
    <img src="${data.icon}" alt="${data.name} logo">
  </div>
  <div class="wordmark-das">
    ${data.name}
  </div>
</header>
    `;
    return htmlHeader;
  },
  panel: function (data) {
    const id = data.id;
    let htmlPanel = `
<form id="customization" onsubmit="saveCustomization()"></form>
${buildFormSection(data.sections, id)}
<hr>`;
    return htmlPanel;
  },
  section: function (data) {
    let htmlSections = '';
    data.sections.forEach(section => {
      htmlSections += buildSection(section);
    });
    return htmlSections;
  }
}
