var db = JSON.parse(localStorage.getItem('simple-dashboard-maker'));

//#region PRIVATE
/**
* Generates and loads the HTML.
*/
function loadHtml(data) {
  // TODO extend to be available for multiple pages
  const page = data.pages[0];
  document.getElementById('header').innerHTML = Builder.header(page);
  document.getElementById('panel').innerHTML = Builder.panel(page);
  document.getElementById('sections').innerHTML = Builder.section(page);
}

/**
* Creates new app object.
* @param {number} [id]
*/
function newApp(id = 0) {
  return {
    id,
    image: '',
    link: '',
    title: ''
  };
}

function newSection(id = 0) {
  return {
    id,
    title: '',
    apps: [newApp()]
  };
}
//#endregion PRIVATE

//#region PUBLIC
/**
* Saves the customization.
*/
saveCustomization = function () {
  const forms = document.forms;
  const pageId = 0;

  for (let ff = 1; ff < forms.length; ff++) {
    const form = forms.item(ff);
    const elements = form.elements;
    const id = form.id;
    const ids = id.match(/\d+/g);
    // TODO extend to pages
    // [section]
    // [section][app]

    const secId = ids.length >= 1 ? ids[0] : null;
    const appId = ids.length >= 2 ? ids[1] : null;

    switch (ids.length) {
      case 1:
        const titleIndex = `sectionTitle[${secId}]`;
        db.pages[pageId].sections[secId].title = elements[titleIndex].value;
        break;
      case 2:
        const imageAppIndex = `appImage[${secId}][${appId}]`;
        const linkAppIndex = `appLink[${secId}][${appId}]`;
        const titleAppIndex = `appTitle[${secId}][${appId}]`;
        db.pages[pageId].sections[secId].apps[appId].image = elements[imageAppIndex].value;
        db.pages[pageId].sections[secId].apps[appId].link = elements[linkAppIndex].value;
        db.pages[pageId].sections[secId].apps[appId].title = elements[titleAppIndex].value;
        break;
      case 3:
        // does exist yet
        db.pages[pageId].sections[ids[0]].title = form.value;
        break;
    }
  }

  localStorage.setItem('simple-dashboard-maker', JSON.stringify(db));
}

addApp = function (pageId, secId) {
  const id = db.pages[pageId].sections[secId].apps.length;

  db.pages[pageId].sections[secId].apps.push(newApp(id));
  localStorage.setItem('simple-dashboard-maker', JSON.stringify(db));
}

deleteApp = function (pageId, secId, id) {
  let apps = db.pages[pageId].sections[secId].apps;
  const ii = apps.findIndex(el => el.id === id);
  apps.splice(ii, 1);

  // catches empty app list
  if (apps.length === 0) {
    apps = [newApp()];
  }

  db.pages[pageId].sections[secId].apps = apps;
}

addSection = function (pageId) {
  const id = db.pages[pageId].sections.length;

  db.pages[pageId].sections.push(newSection(id));
  localStorage.setItem('simple-dashboard-maker', JSON.stringify(db));
}

deleteSection = function (pageId, secId) {
  let sections = db.pages[pageId].sections;
  const ii = sections.findIndex(el => el.id === secId);
  sections.splice(ii, 1);

  // catches empty section list
  if (sections.length === 0) {
    sections = [newSection()];
  }

  db.pages[pageId].sections = sections;
}

/**
* Inits the customization.
*/
initCustomization = function () {
  db = {};
  db.pages = [
    {
      id: 0,
      name: 'Simple Dashboard Maker',
      icon: 'https://barefootstache.codeberg.page/Simple-Dashboard-Maker/@pages/assets/icons/logo.svg',
      sections: [
        {
          id: 0,
          title: 'Example Section',
          apps: [
            {
              id: 0,
              image: 'https://upload.wikimedia.org/wikipedia/commons/a/a9/Example.jpg',
              link: 'https://example.com/',
              title: 'Example'
            }
          ]
        }
      ]
    }
  ];

  localStorage.setItem('simple-dashboard-maker', JSON.stringify(db));
  loadHtml(db);
}

/**
* Loads the customization.
*/
loadCustomization = function () {
  // checks if database exists or init it
  if (db) {
    loadHtml(db);
  } else {
    // inits db
    initCustomization();
  }
}

//#endregion PUBLIC

// init
loadCustomization();
